import socket
import sys
import re
import os
import threading
import errno
import time
import json
import uuid

LOG_FLAG = False
BUFFER_SIZE = 2048

def modify_headers(client_data):
    ''' modify header as specified in the spec''' 
    client_data = re.sub("keep-alive","close", client_data)
    client_data = re.sub("HTTP/1..","HTTP/1.0", client_data)
    return client_data # return the new data with the updated header

def parse_server_info(client_data):
    ''' parse server info from client data and
    returns 4 tuples of (server_ip, server_port, hostname, isCONNECT) '''
    status_line = client_data.split("\n")[0]
    URL = status_line.split(" ")[1]

    if "http://" in URL or ":80" in URL:
        server_port = 80

    if "https://" in URL or ":443" in URL:
        server_port = 443

        if "CONNECT" in status_line: # CONNECT request found
            hostname = URL.split(":")[0]
            server_ip = socket.gethostbyname(hostname)
            return (server_ip, 443, hostname, True) # For a CONNECT request

    hostname = URL.split(":")[1][2:].split("/")[0]
    server_ip = socket.gethostbyname(hostname)

    return (server_ip, server_port, hostname, False) # NOT a CONNECT request


# Creates a subdirectory for the hostname and a new json file
def create_log(hostname, incoming_header, modified_header, server_response):
    pathname = "Log/" + hostname
    if not os.path.exists(pathname):
        os.makedirs(pathname, 0o777, exist_ok=True)
        os.chmod('Log', 0o777)
        os.chmod(pathname, 0o777)
    
    json_dict = {
        'Incoming header': incoming_header,
        'Modified header': modified_header,
        'Server response received' : server_response
    }
    #Dir/Subdir/hostnameuuid.json
    with open(pathname + "/" + hostname + str(uuid.uuid1()) + ".json", "w+") as outfile:
        json.dump(json_dict, outfile, indent=4)

# Creates a subdirectory for the hostname and a new json file (Use this for CONNECT requests)
def create_log2(hostname, incoming_header, response_sent):
    pathname = "Log/" + hostname
    if not os.path.exists(pathname):
        os.makedirs(pathname, 0o777, exist_ok=True)
        os.chmod('Log', 0o777)
        os.chmod(pathname, 0o777)

    json_dict = {
        'Incoming header': incoming_header,
        'Proxy response sent': response_sent,
    }
    #Dir/Subdir/hostnameuuid.json
    with open(pathname + "/" + hostname + str(uuid.uuid1()) + ".json", "w+") as outfile:
        json.dump(json_dict, outfile, indent=4)

# Tunneling method: whatever message received from "from_socket" send to "to_socket" 
# should be used for CONNECT request
def tunnel(from_socket, to_socket):
    while True:
        try:
            to_socket.sendall(from_socket.recv(BUFFER_SIZE))
        except:
            # close sockets when done or when error
            from_socket.close()
            to_socket.close()
            return
        

def proxy(client_socket, client_IP):
    # receive the get request from the client at the proxy, modify the header, and obtain the server info
    get_request = client_socket.recv(BUFFER_SIZE)
    parsed_get = parse_server_info(get_request.decode('utf-8','backslashreplace'))
    
    # print out the first line of the client request
    decoded_get = get_request.decode('utf-8','backslashreplace')
    first_line = decoded_get.split("\n")[0]
    print('>>>', first_line)

    # check for a CONNECT request
    if parsed_get[3] == True:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as tunneling_socket:
            try:
                tunneling_socket.connect((parsed_get[0], parsed_get[1]))  # connect to the server
                server_response = "HTTP/1.1 200 OK"
                client_socket.send_(server_response.encode('utf-8'))  # send OK response if no exceptions are raised
                # start two threads to perform tunneling
                tunnel1 = threading.Thread(target=tunnel, args=(client_socket, tunneling_socket,))
                tunnel2 = threading.Thread(target=tunnel, args=(tunneling_socket, client_socket,))
                tunnel1.start()
                tunnel2.start()
            except:
                server_response = "HTTP/1.1 502 Bad Gateway"
                client_socket.sendall(server_response.encode('utf-8'))  # send a 502 Bad Gateway if exception
                tunneling_socket.close()
                return
    else:
        # modify the header if it is not a CONNECT request
        get_request_mod = modify_headers(get_request.decode('utf-8','backslashreplace'))

        # create a socket at the proxy to communicate with the server
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as server_socket:
            server_socket.connect((parsed_get[0], parsed_get[1]))  # connect to server ip and port
            server_socket.sendall(get_request_mod.encode('utf-8'))  # send modified get request from the client to the server
            # receive the server's response and stream to the client
            server_update = server_socket.recv(BUFFER_SIZE)
            first_update = server_update
            while len(server_update) != 0:
                client_socket.sendall(server_update)
                server_update = server_socket.recv(BUFFER_SIZE)
            server_socket.close()  # close the socket with the server
            client_socket.close()  # close the socket with the client
    
    # log CONNECT requests and non-CONNECT requests accordingly
    global LOG_FLAG
    if LOG_FLAG == True:
        if parsed_get[3] == True:
            create_log2(parsed_get[2], get_request.decode('utf-8','backslashreplace'), server_response)
        else:
            create_log(parsed_get[2], get_request.decode('utf-8','backslashreplace'), get_request_mod, 
            first_update.decode('utf-8','backslashreplace'))
    pass


def main():
    # check arguments
    if(len(sys.argv)!=2 and len(sys.argv)!=3):
        print("Incorrect number of arguments. \nUsage python3 http_proxy.py PORT")
        print("Incorrect number of arguments. \nUsage python3 http_proxy.py PORT Log")
        sys.exit()

    # enable logging
    if(len(sys.argv)==3 and sys.argv[2]=="Log"):
        global LOG_FLAG
        LOG_FLAG = True
        DIR_NAME = "./Log"
        if not (os.path.isdir(DIR_NAME)):
            os.system("mkdir Log")


    # create the socket for this proxy
    proxy_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    proxy_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    
    # bind with the port number given and allow connections
    print ("HTTP proxy listening on port", sys.argv[1])
    proxy_socket.bind(('', int(sys.argv[1])))
    proxy_socket.listen(50) #allow connections

    try: 
        while True:
            client_socket, client_IP = proxy_socket.accept()
            t = threading.Thread(target=proxy, args=(client_socket,client_IP,))
            t.start()
    except KeyboardInterrupt: # handle Ctrl+C
        print ("Keyboard Interrupt: Closing down proxy")
        proxy_socket.close()
        os._exit(1)

if __name__ == "__main__":
    main()