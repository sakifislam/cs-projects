###############################################################################
# client-python.py
# Name: Sakif Islam
# EID: isi72
###############################################################################

import sys
import socket

# the largest amount of data that can be sent to server at once
SEND_BUFFER_SIZE = 2048

def client(server_ip, server_port):
    """TODO: Open socket and send message from sys.stdin"""
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as client_socket:  # open client socket
        client_socket.connect((server_ip, server_port))  # connect socket to server ip and port
        # begin data exchange
        if not sys.stdin.isatty():  # handle input from files
            chunk = sys.stdin.read(SEND_BUFFER_SIZE)
            while chunk: # continue sending chunks of up to 2048 bytes until EOF
                client_socket.sendall(bytes(chunk, 'utf-8'))  # send chunk to server
                chunk = sys.stdin.read(SEND_BUFFER_SIZE)  # obtain new chunk of data
        else:  # handle user input the same way as file input
            line = sys.stdin.readline(SEND_BUFFER_SIZE)
            while line:
                client_socket.sendall(bytes(line, 'utf-8'))
                line = sys.stdin.readline(SEND_BUFFER_SIZE)
                if line == '\n':  # break upon newline user input
                    break
        # data exchange complete
        client_socket.close()  # close the socket


def main():
    """Parse command-line arguments and call client function """
    if len(sys.argv) != 3:
        sys.exit("Usage: python3 client-python.py [Server IP] [Server Port] < [message]")
    server_ip = sys.argv[1]
    server_port = int(sys.argv[2])
    client(server_ip, server_port)

if __name__ == "__main__":
    main()