###############################################################################
# server-python.py
# Name: Sakif Islam
# EID: isi72
###############################################################################

import sys
import socket
import threading

# the largest amount of data that can be received by the server at once
RECV_BUFFER_SIZE = 2048
# the maximum number of clients that can connect at once
QUEUE_LENGTH = 10

def server(service_socket):
    """TODO: Listen on socket and print received message to sys.stdout"""
    while True:
        data = service_socket.recv(RECV_BUFFER_SIZE)  # receive data from client
        if not data: break  # close socket when there is no more data from client
        sys.stdout.write(data.decode("utf-8"))  # write data to stdout


def main():
    """Parse command-line argument and call server function """
    if len(sys.argv) != 2:
        sys.exit("Usage: python3 server-python.py [Server Port]")
    server_port = int(sys.argv[1])
    
    # create a listening socket on the server and bind it to the server port
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as listen_socket:
        listen_socket.bind(('', server_port))
        #print('Listening...')
        listen_socket.listen(QUEUE_LENGTH)
        while True:  # service socket will listen indefinitely until external signal (like Ctrl-C)
            (service_socket, conn_addr) = listen_socket.accept()  # accept new connections from clients
            #print('Connected by:', conn_addr[0] + ",", conn_addr[1])
            worker_thread = threading.Thread(target=server, args=(service_socket,))  # create a new thread to perform work for each client (multithreading)
            #print('New Thread Created')
            worker_thread.start()

if __name__ == "__main__":
    main()